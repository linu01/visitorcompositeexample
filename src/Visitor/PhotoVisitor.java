package Visitor;

import Composite.AreaComponent;

public class PhotoVisitor extends Visitor {

	@Override
	public void visit(City city) {
		// TODO Auto-generated method stub
		System.out.println("Photo Visitor: City take picture");
	}

	@Override
	public void visit(Park park) {
		// TODO Auto-generated method stub
		System.out.println("Photo Visitor: Park take picture");

	}

	@Override
	public void visit(School school) {
		// TODO Auto-generated method stub
		System.out.println("Photo Visitor: School take picture");

	}

	@Override
	public void visit(AreaComponent areaComponent) {
		// TODO Auto-generated method stub
		System.out.println("Photo Visitor: " + areaComponent.getName());

	}

}
