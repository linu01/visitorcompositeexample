package Visitor;

import Composite.*;

public abstract class Visitor {
	public abstract void visit(City city);
	public abstract void visit(Park park);
	public abstract void visit(School school);
	public abstract void visit(AreaComponent areaComponent);
}
