package Visitor;
import Composite.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		AreaComponent yunlin = new AreaComposite("Yunlin");
		AreaComponent taipei = new AreaComposite("Taipei");
		AreaComponent taiwan = new AreaComposite("Taiwan");
		AreaComponent doliu = new AreaLeaf("Doliu");
		AreaComponent huwei = new AreaLeaf("Huwei");
		AreaComponent wanhua = new AreaLeaf("Wanhua");
		AreaComponent banchao = new AreaLeaf("Banchao");

		taipei.addChild(wanhua);
		taipei.addChild(banchao);
		yunlin.addChild(doliu);
		yunlin.addChild(huwei);
		taiwan.addChild(taipei);
		taiwan.addChild(yunlin);
//		taiwan.print();
		PhotoVisitor photo = new PhotoVisitor();
		
		taiwan.accept(photo);
		
		System.out.println("- - - - - -");

		City city = new City();
		city.accept(photo);

		System.out.println("- - - - - -");
		Park park = new Park();
		BuyVisitor buy = new BuyVisitor();
		park.accept(buy);
	}

}
