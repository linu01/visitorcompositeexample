package Visitor;

import Composite.AreaComponent;

public class BuyVisitor extends Visitor{

	@Override
	public void visit(City city) {
		// TODO Auto-generated method stub
		System.out.println("Buy Visitor: City buy");
	}

	@Override
	public void visit(Park park) {
		// TODO Auto-generated method stub
		System.out.println("Buy Visitor: Park buy");
	}

	@Override
	public void visit(School school) {
		// TODO Auto-generated method stub
		System.out.println("Buy Visitor: school buy");

	}

	@Override
	public void visit(AreaComponent areaComponent) {
		// TODO Auto-generated method stub
		System.out.println("Buy Visitor: " +areaComponent.getName());

	}

}
