package Composite;
import Visitor.*;
public abstract class AreaComponent {
	protected String name;

	public AreaComponent(String name) {
		super();
		this.name = name;
	}

	public abstract void accept(Visitor visitor);

	public void addChild(AreaComponent areaComponent) {
	}

	public void remove(AreaComponent areaComponent) {
	}

	public abstract void print();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
