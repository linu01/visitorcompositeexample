package Composite;

import java.util.ArrayList;

import Visitor.Visitor;

public class AreaComposite extends AreaComponent {
	ArrayList<AreaComponent> areaArray = new ArrayList<AreaComponent>();

	public AreaComposite(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public void addChild(AreaComponent areaComponent) {
		areaArray.add(areaComponent);
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		System.out.println(name);
		for (AreaComponent areaComponent : areaArray) {
			areaComponent.print();
		}
	}

	@Override
	public void accept(Visitor visitor) {
		// TODO Auto-generated method stub
		visitor.visit(this);
	}
}
